//
//  GameViewController.h
//  Bejeweled_Clone
//
//  Created by Matt Perl on 3/26/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GameViewController : UIViewController

@property (strong, nonatomic) IBOutletCollection(UIButton) NSMutableArray *GridButtons;
@property (strong, nonatomic) IBOutlet UITextField *CSTextField;
@property (strong, nonatomic) IBOutlet UITextField *HSTextField;


@property (strong, nonatomic) IBOutlet UIButton *firstButtonPressed;
@property (strong, nonatomic) IBOutlet UIButton *secondButtonPressed;

@property (nonatomic) int numMovesLeft;

@end
