//
//  ViewController.m
//  Bejeweled_Clone
//
//  Created by Matt Perl on 3/26/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) tapPlayButton:(id) sender{
    
    [self performSegueWithIdentifier:@"TransitionToGame" sender:self];
}

@end
