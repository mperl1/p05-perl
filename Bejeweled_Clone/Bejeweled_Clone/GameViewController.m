//
//  GameViewController.m
//  Bejeweled_Clone
//
//  Created by Matt Perl on 3/26/17.
//  Copyright © 2017 Matt Perl. All rights reserved.
//

#import "GameViewController.h"

@interface GameViewController ()

@end

int gridMap[5][4] = {{-1, -1, -1, -1},
    {-1, -1, -1, -1},
    {-1, -1, -1, -1},
    {-1, -1, -1, -1},
    {-1, -1, -1, -1}};

int buttonClicks = 0;
bool button1Matched = false;
bool button2Matched = false;

int currentScore = 0;

@implementation GameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializeGrid];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction) tapMainMenuButton:(id) sender{
    
    [self performSegueWithIdentifier:@"TransitionToStartScreen" sender:self];
}

- (void) initializeGrid{
    int randImg;
    int counter = 0;
    int ind1;
    int ind2;
    for(UIButton *gridPos in _GridButtons){
        randImg = arc4random_uniform(4);
        
        if(counter < 4){
            ind1 = 0;
        }
        else if(counter < 8){
            ind1 = 1;
        }
        else if(counter < 12){
            ind1 = 2;
        }
        else if(counter < 16){
            ind1 = 3;
        }
        else{
            ind1 = 4;
        }
        ind2 = counter%4;
        
        //printf("%d, %d\n", ind1, ind2);
        UIImage *tmpImg;
        switch(randImg){
            case 0:
                tmpImg = [UIImage imageNamed:@"diamond.png"];
                [tmpImg setAccessibilityValue:@"0"];
                [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                gridMap[ind1][ind2] = 0;
                break;
            case 1:
                tmpImg = [UIImage imageNamed:@"ruby.png"];
                [tmpImg setAccessibilityValue:@"1"];
                [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                gridMap[ind1][ind2] = 1;
                break;
            case 2:
                tmpImg = [UIImage imageNamed:@"amethyst.png"];
                [tmpImg setAccessibilityValue:@"2"];
                [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                gridMap[ind1][ind2] = 2;
                break;
            case 3:
                tmpImg = [UIImage imageNamed:@"citrine.png"];
                [tmpImg setAccessibilityValue:@"3"];
                [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                gridMap[ind1][ind2] = 3;
                break;
        }
        counter++;
    }
    
    /*
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 4; j++){
            printf("%d", gridMap[i][j]);
        }
        printf("\n");
    }
     */
}

- (IBAction)gemButtonPressed:(id)sender{
    if(0 == buttonClicks){
        _firstButtonPressed = sender;
        buttonClicks++;
        //NSLog(@"The button title is %@", _firstButtonPressed.currentTitle);
    }
    else{
        _secondButtonPressed = sender;
        
        int but1Ind1 = [_firstButtonPressed.currentTitle characterAtIndex:0]-48;
        int but1Ind2 = [_firstButtonPressed.currentTitle characterAtIndex:1]-48;
        int but2Ind1 = [_secondButtonPressed.currentTitle characterAtIndex:0]-48;
        int but2Ind2 = [_secondButtonPressed.currentTitle characterAtIndex:1]-48;
        
        //printf("%d, %d, %d, %d\n", but1Ind1, but1Ind2, but2Ind1, but2Ind2);
        
        if(_firstButtonPressed != _secondButtonPressed
           && ((but1Ind1-1 == but2Ind1 && but1Ind2 == but2Ind2)
               || (but1Ind1+1 == but2Ind1 && but1Ind2 == but2Ind2)
               || (but1Ind1 == but2Ind1 && but1Ind2-1 == but2Ind2)
               || (but1Ind1 == but2Ind1 && but1Ind2+1 == but2Ind2)))
        {
            //int backupVal1 = gridMap[but1Ind1][but1Ind2];
            //int backupVal2 = gridMap[but2Ind1][but2Ind2];
            
            int newVal1 = [_firstButtonPressed.currentBackgroundImage.accessibilityValue intValue];
            int newVal2 = [_secondButtonPressed.currentBackgroundImage.accessibilityValue intValue];
            
            //NSLog(@"%d, %d", newVal1, newVal2);
            
            button1Matched = [self checkBut1Matches:but1Ind1 :but1Ind2 :but2Ind1 :but2Ind2];
            button2Matched = [self checkBut2Matches:but1Ind1 :but1Ind2 :but2Ind1 :but2Ind2];
            
            //int swapEntered = 0;
            if(button1Matched || button2Matched){
                //printf("here\n");
                /*
                for(int i = 0; i < 5; i++){
                    for(int j = 0; j < 4; j++){
                        printf("%d", gridMap[i][j]);
                    }
                    printf("\n");
                }
                printf("\n");
                */
                
                gridMap[but1Ind1][but1Ind2] = newVal2;
                gridMap[but2Ind1][but2Ind2] = newVal1;
                
                UIImage *tempBackgroundImg;
                UIImage *tempImg;
                
                tempBackgroundImg = [_firstButtonPressed currentBackgroundImage];
                tempImg = [_firstButtonPressed currentImage];
                
                [_firstButtonPressed setBackgroundImage:_secondButtonPressed.currentBackgroundImage forState:UIControlStateNormal];
                [_firstButtonPressed setImage:_secondButtonPressed.currentImage forState:UIControlStateNormal];
                [_secondButtonPressed setBackgroundImage:tempBackgroundImg forState:UIControlStateNormal];
                [_secondButtonPressed setImage:tempImg forState:UIControlStateNormal];
                
                //handle moving behavior in new functions
                /*
                for(int i = 0; i < 5; i++){
                    for(int j = 0; j < 4; j++){
                        printf("%d", gridMap[i][j]);
                    }
                    printf("\n");
                }
                printf("\n");
                */
                
                if(button1Matched && button2Matched){
                    [self adjustGridVals:but1Ind1 :but1Ind2];
                    [self adjustGridVals:but2Ind1 :but2Ind2];
                }
                else if(button1Matched){
                    [self adjustGridVals:but2Ind1 :but2Ind2];
                }
                else{
                    [self adjustGridVals:but1Ind1 :but1Ind2];
                }
                
                /*
                for(int i = 0; i < 5; i++){
                    for(int j = 0; j < 4; j++){
                        printf("%d", gridMap[i][j]);
                    }
                    printf("\n");
                }
                printf("\n");
                */
                
                [self shiftValsDown];
                
                /*
                for(int i = 0; i < 5; i++){
                    for(int j = 0; j < 4; j++){
                        printf("%d", gridMap[i][j]);
                    }
                    printf("\n");
                }
                printf("\n");
                */
                
                [self refreshGrid];
                
                [self repopGrid];
                
                /*
                for(int i = 0; i < 5; i++){
                    for(int j = 0; j < 4; j++){
                        printf("%d", gridMap[i][j]);
                    }
                    printf("\n");
                }
                printf("\n");
                */
                
                //swapEntered++;
                button1Matched = false;
                button2Matched = false;
            }
            
            //printf("%d\n", loopCycles);
            /*
            if(swapEntered == 0){
                gridMap[but1Ind1][but1Ind2] = backupVal1;
                gridMap[but2Ind1][but2Ind2] = backupVal2;
            }
             */
        }
        buttonClicks = 0;
    }
}

/**
 * Updates the buttons according to the grid
 */
- (void) refreshGrid{
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 4; j++){
            
            for(UIButton *gridPos in _GridButtons){
                if([gridPos.currentTitle characterAtIndex:0]-48 == i
                   && [gridPos.currentTitle characterAtIndex:1]-48 == j){
                    
                    int currentVal = gridMap[i][j];
                    UIImage *tmpImg;
                    
                    switch(currentVal){
                        case 0:
                            tmpImg = [UIImage imageNamed:@"diamond.png"];
                            [tmpImg setAccessibilityValue:@"0"];
                            [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                            gridMap[i][j] = 0;
                            break;
                        case 1:
                            tmpImg = [UIImage imageNamed:@"ruby.png"];
                            [tmpImg setAccessibilityValue:@"1"];
                            [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                            gridMap[i][i] = 1;
                            break;
                        case 2:
                            tmpImg = [UIImage imageNamed:@"amethyst.png"];
                            [tmpImg setAccessibilityValue:@"2"];
                            [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                            gridMap[i][j] = 2;
                            break;
                        case 3:
                            tmpImg = [UIImage imageNamed:@"citrine.png"];
                            [tmpImg setAccessibilityValue:@"3"];
                            [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                            gridMap[i][j] = 3;
                            break;
                    }
                    //break;
                }
            }
        }
    }
    
}

/**
 * Insert -1 into grid position where matches happened
 */
- (void) adjustGridVals:(int) ind1 :(int) ind2{
    
    int upCount = 0;
    int leftCount = 0;
    int rightCount = 0;
    int downCount = 0;
    
    int iter = ind1;
    while(iter >= 0){
        if(gridMap[ind1][ind2] == gridMap[iter][ind2]){
            upCount++;
        }
        else{
            break;
        }
        iter--;
    }
    
    iter = ind1;
    while(iter < 5){
        if(gridMap[ind1][ind2] == gridMap[iter][ind2]){
            downCount++;
        }
        else{
            break;
        }
        iter++;
    }
    
    iter = ind2;
    while(iter >= 0){
        if(gridMap[ind1][ind2] == gridMap[ind1][iter]){
            leftCount++;
        }
        else{
            break;
        }
        iter--;
    }
    
    iter = ind2;
    while(iter < 4){
        if(gridMap[ind1][ind2] == gridMap[ind1][iter]){
            rightCount++;
        }
        else{
            break;
        }
        iter++;
    }
    
    if((upCount+downCount) >= 3){
        iter = ind1;
        while(upCount > 0){
            gridMap[iter][ind2] = -1;
            iter--;
            upCount--;
        }
        
        iter = ind1;
        while(downCount > 0){
            gridMap[iter][ind2] = -1;
            iter++;
            downCount--;
        }
    }
    
    if((leftCount+rightCount) >= 3){
        iter = ind2;
        while(leftCount > 0){
            gridMap[ind1][iter] = -1;
            iter--;
            leftCount--;
        }
        
        iter = ind2;
        while(rightCount > 0){
            gridMap[ind1][iter] = -1;
            iter++;
            rightCount--;
        }
    }
    
    /*
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 4; j++){
            printf("%d", gridMap[i][j]);
        }
        printf("\n");
    }
    printf("\n");
     */
}

/**
 * Shift blocks down to fill empty slots
 */
- (void) shiftValsDown{
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 4; j++){
            
            if(gridMap[i][j] == -1){
                int ind = i;
                while(gridMap[ind-1][j] != -1 && ind > 0){
                    int temp = gridMap[ind-1][j];
                    gridMap[ind][j] = temp;
                    gridMap[ind-1][j] = -1;
                    ind--;
                }
            }
        }
    }
}

/**
 * Repopulate empty slots with random gems
 */
 - (void) repopGrid{
     int numConverted = 0;
     int randImg;
     UIImage *tmpImg;
 
     for(int i = 0; i < 5; i++){
         for(int j = 0; j < 4; j++){
             if(gridMap[i][j] == -1){
                 randImg = arc4random_uniform(4);
 
                 for(UIButton *gridPos in _GridButtons){
                     if([gridPos.currentTitle characterAtIndex:0]-48 == i
                        && [gridPos.currentTitle characterAtIndex:1]-48 == j){
                         numConverted++;
                         switch(randImg){
                             case 0:
                                 tmpImg = [UIImage imageNamed:@"diamond.png"];
                                 [tmpImg setAccessibilityValue:@"0"];
                                 [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                                 gridMap[i][j] = 0;
                                 break;
                             case 1:
                                 tmpImg = [UIImage imageNamed:@"ruby.png"];
                                 [tmpImg setAccessibilityValue:@"1"];
                                 [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                                 gridMap[i][j] = 1;
                                 break;
                             case 2:
                                 tmpImg = [UIImage imageNamed:@"amethyst.png"];
                                 [tmpImg setAccessibilityValue:@"2"];
                                 [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                                 gridMap[i][j] = 2;
                                 break;
                             case 3:
                                 tmpImg = [UIImage imageNamed:@"citrine.png"];
                                 [tmpImg setAccessibilityValue:@"3"];
                                 [gridPos setBackgroundImage:tmpImg forState: UIControlStateNormal];
                                 gridMap[i][j] = 3;
                                 break;
                         }
                     }
                 }
             }
         }
     }
     currentScore += numConverted*500;
     [_CSTextField setText:[@(currentScore) stringValue]];
 }

/**
 * Check to see if any moves are possible
 */
/*
- (bool) anyMovesRemaining{
    //Check to see if there are any moves possible.
    int ind1 = 0;
    int ind2 = 0;
    */
    //Create copy of grip map to manipulate
    /*
    int tmpGrid[5][4];
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 4; j++){
            tmpGrid[i][j] = gridMap[i][j];
        }
    }*/

    //TO-DO
    //Need to rewrite the logic here
/*
    while(ind1 < 5){
        while(ind2 < 4){
 
            //Check move up
            if(ind1 > 0 && ind2 > 0 && ind2 < 3){
                if(gridMap[ind1-1][ind2-1] == gridMap[ind1][ind2]
                   && gridMap[ind1-1][ind2+1] == gridMap[ind1][ind2]){
                    return true;
                }
            }
 
            //Check move left
            if(ind1 > 0 && ind1 < 4 && ind2 > 0){
                if(gridMap[ind1-1][ind2-1] == gridMap[ind1][ind2]
                   && gridMap[ind1+1][ind2-1] == gridMap[ind1][ind2]){
                    return true;
                }
            }
            
            //Check move right
            if(ind1 > 0 && ind1 < 4 && ind2 < 3){
                if(gridMap[ind1-1][ind2+1] == gridMap[ind1][ind2]
                   && gridMap[ind1+1][ind2+1]){
                    return true;
                }
            }
            
            //Check move down
            if(ind1 < 4 && ind2 > 0 && ind2 < 3){
                if(gridMap[ind1+1][ind2-1] == gridMap[ind1][ind2]
                   && gridMap[ind1+1][ind2+1] == gridMap[ind1][ind2]){
                    return true;
                }
            }
             
            
            ind2++;
        }
        ind1++;
    }
    
    return false;
}*/

/**
 * If 2 buttons have been pressed, check if button 2 is a valid move.
 * Note: Only checks for the minimum combination since this method will not
 * manipulate the grid directly
 */
- (bool) checkBut2Matches:(int) ind1 :(int) ind2 :(int) ind3 :(int) ind4{
    
    //Right check
    if(ind4 == 0 || (ind4 == 1 && ind2-ind4 == 0)){ //check two right 2nd button
        if(gridMap[ind1][ind2+1] == gridMap[ind3][ind4]
           && gridMap[ind1][ind2+2] == gridMap[ind3][ind4]){
            return true;
        }
    }
    
    //Left check
    if(ind4 == 3 || (ind4 == 2 && ind2-ind4 == 0)){ //check two left 2nd button
        if(gridMap[ind1][ind2-1] == gridMap[ind3][ind4]
           && gridMap[ind1][ind2-2] == gridMap[ind3][ind4]){
            return true;
        }
    }
    
    //Left and Right check
    if(ind4 > 0 && ind4 < 3 && ind2-ind4 == 0){ //check left and right 2nd button
        if(gridMap[ind1][ind2-1] == gridMap[ind3][ind4]
           && gridMap[ind1][ind2+1] == gridMap[ind3][ind4]){
            return true;
        }
    }
    
    //Down check
    if(ind3 == 0 || ind3 == 1 || (ind3 == 2 && (ind3 >= ind1))){//check two down 2nd button
        if(gridMap[ind1+1][ind2] == gridMap[ind3][ind4]
           && gridMap[ind1+2][ind2] == gridMap[ind3][ind4]
           && ind3 - ind1 != 1){
            return true;
        }
    }
    
    //Up check
    if(ind3 == 4 || ind3 == 3 || (ind3 == 2 && (ind3 <= ind1))){//check two up 2nd button
        if(gridMap[ind1-1][ind2] == gridMap[ind3][ind4]
           && gridMap[ind1-2][ind2] == gridMap[ind3][ind4]
           && ind1 - ind3 != 1){
            return true;
        }
    }
    
    //Up and Down check
    if(ind3 > 0 && ind3 < 4 && ind1-ind3 == 0){//check up and down 2nd button
        if(gridMap[ind1-1][ind2] == gridMap[ind3][ind4]
           && gridMap[ind1+1][ind2] == gridMap[ind3][ind4]){
            return true;
        }
    }

    return false;
}

/**
 * If 2 buttons have been pressed, check if button 1 is a valid move.
 * Note: Only checks for the minimum combination since this method will not
 * manipulate the grid directly
 */
- (bool) checkBut1Matches:(int)ind1 :(int) ind2 :(int) ind3 :(int) ind4{
    
    //Right check
    if(ind2 == 0 || (ind2 == 1 && ind2-ind4 == 0)){ //check two right 1st button
        if(gridMap[ind3][ind4+1] == gridMap[ind1][ind2]
           && gridMap[ind3][ind4+2] == gridMap[ind1][ind2]){
            return true;
        }
    }
    
    //Left check
    if(ind2 == 3 || (ind2 == 2 && ind2-ind4 == 0)){//check two left 1st button
        if(gridMap[ind3][ind4-1] == gridMap[ind1][ind2]
           && gridMap[ind3][ind4-2] == gridMap[ind1][ind2]){
            return true;
        }
    }
    
    //Left and Right check
    if(ind2 > 0 && ind2 < 3 && ind2-ind4 == 0){ //check left and right 1st button
        if(gridMap[ind3][ind4-1] == gridMap[ind1][ind2]
           && gridMap[ind3][ind4+1] == gridMap[ind1][ind2]){
            return true;
        }
    }
    
    //Down check
    if(ind1 == 0 || ind1 == 1 || (ind1 == 2 && (ind1 >= ind3))){ //check two down 1st button
        if(gridMap[ind3+1][ind4] == gridMap[ind1][ind2]
           && gridMap[ind3+2][ind4] == gridMap[ind1][ind2]
           && ind1 - ind3 != 1){
            return true;
        }
    }
    
    //Up check
    if(ind1 == 4 || ind1 == 3 || (ind1 == 2 && (ind1 <= ind3))){ //check two up 1st button
        if(gridMap[ind3-1][ind4] == gridMap[ind1][ind2]
           && gridMap[ind3-2][ind4] == gridMap[ind1][ind2]
           && ind3 - ind1 != 1){
            return true;
        }
    }
    
    //Up and Down check
    if(ind1 > 0 && ind1 < 4 && ind1-ind3 == 0){ //check up and down 1st button
        if(gridMap[ind3-1][ind4] == gridMap[ind1][ind2]
           && gridMap[ind3-2][ind4] == gridMap[ind1][ind2]){
            return true;
        }
    }

    return false;
}

- (IBAction) tapResetButton:(id) sender{
    [self initializeGrid];
    
    int hs = [[_HSTextField text] intValue];
    
    if(currentScore > hs){
        //set highscore
        [_HSTextField setText: _CSTextField.text];
    }
    
    //reset currentScore
    [_CSTextField setText:@"0"];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
