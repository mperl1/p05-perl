@author Matt Perl

The goal of this project is to make a bejeweled clone while implementing screen transitions from the landing page and having a game over pop-up.

For now, it will only have a quad-directional functionality. Diagonals will be added if time allows

Known bugs that I didn't have time to fix:
- combo will not always remove every block

Outside sources:
See contributors.txt

Note to self: Should have used spritekit
